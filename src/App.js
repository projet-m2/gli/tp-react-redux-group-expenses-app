import React, { Component } from 'react';
import './App.css';
import ListShopping from './Component/ListShopping';
import FilterUser from './Component/FilterUser';
import AddPurchase from './Component/AddPurchase';
import TotalShopping from './Component/TotalShopping';

import {Provider} from "react-redux"
import {createStore} from "redux"
import rootReducer from "./reducer/rootReducer.js"

const store = createStore(rootReducer);

class App extends React.Component{

  render(){
    return(
      <Provider store={store}>
        <div>
        <div class="App">
          <header class="App-header">
            <h1>Week end Party</h1>
          </header>
        </div>
        <FilterUser/>
        <ListShopping  />
        
        <footer>
        <AddPurchase/>
        <TotalShopping/>
        </footer>
      </div>
    </Provider>
    );
  }

}

export default App;
