const stateInitialisation = {
    costs: [
        { paidBy: 0, paidFor: 'Beer', amount: 15 },
        { paidBy: 0, paidFor: 'Cookies', amount: 4 },
        { paidBy: 1, paidFor: 'Pizza', amount: 10 },
        { paidBy: 1, paidFor: 'Chips', amount: 4 },
        { paidBy: 2, paidFor: 'Chocolate', amount: 3 },
        { paidBy: 2, paidFor: 'BBQ', amount: 20 },
        { paidBy: 3, paidFor: 'Beer', amount: 18 },
        { paidBy: 3, paidFor: 'Vegetable', amount: 5 }
      ],
      users:[
        'Toto',
        'Tata',
        'Titi',
        'Tutu'
      ],
      filterUser: -1
  }

  export function costs(state = stateInitialisation.costs, action){
    switch (action.type){
        case "ADD_COST":
            return [...state, action.cost];
        default:
            return state;
        }
    }
  
  export function filterUser(state = stateInitialisation.filterUser, action){
      switch (action.type){
          case "CHANGE_FILTER":
              return action.filter;
          default:
            return state;
        }
    }
  
  export function users(state = stateInitialisation.users, action){
      switch (action.type){
          case "ADD_USER":
              return [...state, action.user];
          default:
            return state;
        }
    }
  
