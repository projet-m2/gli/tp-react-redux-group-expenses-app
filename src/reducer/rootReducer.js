 
import { costs, users, filterUser } from "./reducer.js"

export default function rootReducer(state = {}, action){
	return {
        costs: costs(state.costs, action),
        users: users(state.users, action),
        filterUser: filterUser(state.filterUser, action)
	}
}