const ADD_COST = 'ADD_COST';
export const addCost = (newPaidBy, newPaidFor, newAmount) => {
    return { type: ADD_COST, cost: { paidBy: newPaidBy, paidFor: newPaidFor, amount: newAmount}}
}

const CHANGE_FILTER= "CHANGE_FILTER";
export const changeFilter = (newFilter) => {
	return { type: CHANGE_FILTER, filter: newFilter};
}

const ADD_USER = "ADD_USER";
export const addUser = (newUser) => {
  return { type: ADD_USER, user: newUser};
}