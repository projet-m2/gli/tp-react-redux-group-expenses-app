import React, { Component } from 'react'
import { connect } from 'react-redux';
import { addCost } from '../reducer/redux-actions'
import { addUser } from '../reducer/redux-actions'

class AddPurchase extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            who: '',
            what: '',
            price: 0
        }
    }

    handleClick = () => {
        if(this.state.who !== '' && this.state.what !== '' && parseInt(this.state.price) > 0){
            if(this.props.users.indexOf(this.state.who) === -1){
                this.props.addUser(this.state.who);
            }
            this.props.addCost(this.props.users.indexOf(this.state.who), this.state.what, this.state.price);
        }
    }

    
    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value })
    }

    render(){
        return (
        <form class="form-inline">
            <input type="text" name="who" placeholder="Paid by ?" onChange={event => this.handleChange(event)} required/>
        
            <input type="text" name="what" placeholder="Paid for ?" onChange={event => this.handleChange(event)} required/>
        
            <input type="number" name="price" placeholder="Amount ?" onChange={event => this.handleChange(event)} required/>
        
            <button type="button" onClick={this.handleClick}>Add</button>
        </form>
        );
    }
}

const mapStateToProps = (state) => {
	return {
		users : state.users
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		addCost: (who, what, price) => {
			dispatch(addCost(who, what, parseInt(price)));
		},
        addUser: (who) => {
			dispatch(addUser(who));
        }
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddPurchase);