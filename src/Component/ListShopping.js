import React, { Component } from 'react'
import { connect } from 'react-redux';

class ListShopping extends React.Component{

    constructor(props){
        super(props);
    }

    listCosts = () =>
        this.props.costs
        .filter(cost => ( cost.paidBy === parseInt(this.props.filterUser) || parseInt(this.props.filterUser) === -1))
        .map((cost) => 
            <tr>
                <td>{this.props.users[cost.paidBy]}</td>
                <td>{cost.paidFor}</td>
                <td>{cost.amount}</td>
            </tr>
        );

    render(){
        return( 
            <table>
                <thead>
                    <tr>
                        <th scope="col">Paid by</th>
                        <th scope="col">Paid for</th>
                        <th scope="col">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {this.listCosts()}
                </tbody>
            </table>
        );
    }

}

const mapStateToProps = (state) => {
	return {
		costs: state.costs,
        users : state.users,
        filterUser: state.filterUser
	}
}
const mapDispatchToProps = (dispatch) => {
	return {}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
) (ListShopping);