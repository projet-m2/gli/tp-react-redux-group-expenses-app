import React, { Component } from 'react'
import { connect } from 'react-redux';
import { changeFilter } from '../reducer/redux-actions';

class FilterUser extends React.Component{

    constructor(props){
        super(props);
    }

    listUser = () =>
    this.props.users
    .map((user, index) => 
        <option value={index}>{user}</option>
    );

    handleChange(event){
        this.props.changeFilter(event.target.value);
    }

    render(){
        return( 
        <form onChange={(event) => this.handleChange(event)}>
            <select>
              <option value="-1">All</option>
              {this.listUser()}
            </select>
        </form>  
        );
    }

}

const mapStateToProps = (state) => {
	return {
		users : state.users
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		changeFilter: (value) => {
			dispatch(changeFilter(value))
		}
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
) (FilterUser);
