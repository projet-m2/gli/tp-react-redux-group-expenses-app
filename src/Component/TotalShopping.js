import React, { Component } from 'react'
import { connect } from 'react-redux';

class TotalShopping extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
    let totalShop = this.props.costs
    .filter(cost => cost.paidBy === parseInt(this.props.filterUser) || parseInt(this.props.filterUser) === -1)
    .reduce((total, value) => total + value.amount, 0)

    return (

            <p>Total : {totalShop}€</p>
    );
    }

}

const mapStateToProps = (state) => {
	return {
        costs: state.costs,
        filterUser: state.filterUser
	 }
}

const mapDispatchToProps = (dispatch) => {
	return {}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
) (TotalShopping);
